<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/registration', 'AuthController@registration');
    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');
    Route::post('/me', 'AuthController@me');
});

Route::group(['prefix' => 'blog', 'namespace' => 'Blog'], function () {
    Route::get('/', 'BlogController@index');
    Route::get('/me', 'BlogController@me');
    Route::get('/{id}', 'BlogController@show')->where('id', '\d+');
    Route::post('/', 'BlogController@create');
    Route::patch('/{id}', 'BlogController@update')->where('id', '\d+');
    Route::delete('/{id}', 'BlogController@delete')->where('id', '\d+');

    Route::group(['prefix' => 'attachments'], function () {
        Route::post('/', 'AttachmentsController@upload');
        Route::get('/{id}', 'AttachmentsController@download')->where('id', '\d+')->name('attachment.download');
        Route::delete('/{id}', 'AttachmentsController@delete')->where('id', '\d+');
    });
});
