<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBlogPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->string('title');
            $table->text('content');
            $table->unsignedInteger('views')->default(0);
            $table->timestamps();

            $table
                ->foreign('user_id', 'blog_posts_user_id_foreign')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });

        Schema::create('blog_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_id');
            $table->string('file_path', 100);
            $table->string('file_name');

            $table
                ->foreign('post_id', 'blog_attachments_post_id_foreign')
                ->references('id')
                ->on('blog_posts')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });

        Schema::create('blog_tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_id');
            $table->string('tag', 32);

            $table
                ->foreign('post_id', 'blog_tags_post_id_foreign')
                ->references('id')
                ->on('blog_posts')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_tags');
        Schema::dropIfExists('blog_attachments');
        Schema::dropIfExists('blog_posts');
    }
}
