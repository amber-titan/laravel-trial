<?php namespace App\Http\Requests\Blog;

use App\Http\Requests\BaseFormRequest;

/**
 * Class UpdateRequest
 *
 * @package App\Http\Requests\Blog
 */
class UpdateRequest extends BaseFormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return !empty(auth()->id());
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "title"   => "max:256",
            "content" => "max:65000",
        ];
    }
}
