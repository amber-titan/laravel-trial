<?php namespace App\Http\Requests\Blog;

use App\Http\Requests\BaseFormRequest;

class MeFilterRequest extends BaseFormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return !empty(auth()->id());
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "sort"       => "in:asc,desc",
            "tag"        => "max:250",
            "attachment" => "in:0,1",
        ];
    }
}
