<?php namespace App\Http\Requests\Blog;

use App\Http\Requests\BaseFormRequest;

class FilterRequest extends BaseFormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "sort"       => "in:asc,desc",
            "user_id"    => "integer|exists:users,id",
            "tag"        => "max:250",
            "attachment" => "in:0,1",
        ];
    }
}
