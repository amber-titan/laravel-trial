<?php namespace App\Http\Requests\Blog;

use App\Http\Requests\BaseFormRequest;

class CreateRequest extends BaseFormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return !empty(auth()->id());
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "title"   => "required|max:256",
            "content" => "required|max:65000",
        ];
    }
}
