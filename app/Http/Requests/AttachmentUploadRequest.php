<?php

declare(strict_types = 1);

namespace App\Http\Requests;

class AttachmentUploadRequest extends BaseFormRequest
{
    public function authorize(): bool
    {
        return !empty(auth()->id());
    }

    public function rules(): array
    {
        return [
            'post_id'    => 'required|integer|exists:blog_posts,id',
            'attachment' => 'required|file',
        ];
    }
}
