<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\CreateRequest;
use App\Http\Requests\Blog\FilterRequest;
use App\Http\Requests\Blog\MeFilterRequest;
use App\Http\Requests\Blog\UpdateRequest;
use App\Models\Blog\Post;
use App\Services\BlogServiceInterface;
use Exception;
use Response;

class BlogController extends Controller
{
    /**
     * @var BlogServiceInterface
     */
    public $blogService;

    /**
     * BlogController constructor.
     *
     * @param BlogServiceInterface $blogService
     */
    public function __construct(BlogServiceInterface $blogService)
    {
        $this->blogService = $blogService;
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * @param FilterRequest $request
     *
     * @return mixed
     */
    public function index(FilterRequest $request)
    {
        $posts = $this->blogService->getPosts($request->all(), $request->get('sort', 'desc'));

        return $this->blogService->postsToArray($posts);
    }

    /**
     * @param MeFilterRequest $request
     * @return JsonResponse
     */
    public function me(MeFilterRequest $request)
    {
        return response()->json($this->blogService->getPosts(
            array_merge($request->all(), ['user_id' => auth()->id()]),
            $request->get('sort', 'desc'))
        );
    }

    /**
     * Create
     *
     * @param CreateRequest $request
     *
     * @return array
     */
    public function create(CreateRequest $request)
    {
        $post = $this->blogService->store($request);

        return $this->blogService->postToArray($post);
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return $this->blogService->postToArray($post);
    }

    /**
     * @param               $id
     * @param UpdateRequest $request
     *
     * @return array|Response
     */
    public function update($id, UpdateRequest $request)
    {
        $post = Post::find($id);

        if($post->user_id != auth()->id()) {
            return response('{}', 403);
        }

        $this->blogService->update($post, $request);

        return $this->blogService->postToArray($post);
    }

    /**
     * @param $id
     *
     * @return Response|string
     *
     * @throws Exception
     */
    public function delete($id)
    {
        $post = Post::findOrFail($id);

        if($post->user_id != auth()->id()) {
            return response('{}', 403);
        }

        $post->delete();

        return '{}';
    }
}
