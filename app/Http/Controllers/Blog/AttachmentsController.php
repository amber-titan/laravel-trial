<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Blog;

use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Controllers\Controller;
use App\Http\Requests\AttachmentUploadRequest;
use App\Services\AttachmentsService;
use Exception;
use Response;
use Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

class AttachmentsController extends Controller
{
    /**
     * @var AttachmentsService
     */
    private $service;

    public function __construct(AttachmentsService $service)
    {
        $this->service = $service;
    }

    /**
     * @param AttachmentUploadRequest $request
     *
     * @return array
     *
     * @throws AuthorizationException
     */
    public function upload(AttachmentUploadRequest $request)
    {
        $postId     = (int) $request->get('post_id');
        $file       = $request->file('attachment');
        $attachment = $this->service->createAttachment($postId, $file);

        return $this->service->attachmentToArray($attachment);
    }

    /**
     * @param int|string $id
     *
     * @return Response|StreamedResponse
     */
    public function download($id)
    {
        if (!$attachment = $this->service->getAttachmentById((int) $id)) {
            return response('{}', 404);
        }

        return Storage::download($attachment->file_path, $attachment->file_name);
    }

    /**
     * @param $id
     *
     * @return Response
     *
     * @throws Exception
     */
    public function delete($id)
    {
        $status = $this->service->deleteAttachmentById((int) $id) ? 200 : 404;

        return response('{}', $status);
    }
}
