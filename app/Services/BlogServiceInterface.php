<?php

namespace App\Services;

use App\Models\Blog\Post;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

interface BlogServiceInterface
{
    /**
     * @param Post $post
     *
     * @return array
     */
    public function postToArray(Post $post);

    /**
     * @param LengthAwarePaginator $posts
     *
     * @return array
     */
    public function postsToArray(LengthAwarePaginator $posts): array;

    /**
     * Создание
     *
     * @param Request $request
     *
     * @return Post|Model
     */
    public function store(Request $request): Post;

    /**
     * @param Post $post
     * @param Request $request
     *
     * @return Post
     */
    public function update(Post $post, Request $request): Post;

    /**
     * @param array $filter
     * @param string $sort
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPosts(array $filter = [], string $sort = 'desc');
}
