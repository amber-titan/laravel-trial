<?php

declare(strict_types = 1);

namespace App\Services;

use App\Models\Blog\Attachment;
use App\Models\Blog\Post;
use App\User;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class AttachmentsService
{
    /**
     * @param Attachment $attachment
     *
     * @return array
     */
    public function attachmentToArray(Attachment $attachment): array
    {
        return [
            'url'       => route('attachment.download', ['id' => $attachment->id]),
            'file_name' => $attachment->file_name,
        ];
    }

    /**
     * @param Collection|Attachment[] $collection
     *
     * @return array
     */
    public function attachmentsToArray(Collection $collection): array
    {
        $result = [];

        foreach ($collection as $attachment) {
            $result[] = $this->attachmentToArray($attachment);
        }

        return $result;
    }

    /**
     * @param int          $postId
     * @param UploadedFile $file
     *
     * @return Attachment
     *
     * @throws AuthorizationException
     */
    public function createAttachment(int $postId, UploadedFile $file): Attachment
    {
        $post = Post::findOrFail($postId);
        $user = auth()->user();

        if (!$user instanceof User) {
            throw new AuthorizationException();
        }
        if ($user->id !== (int) $post->user_id) {
            throw new AccessDeniedException();
        }

        $attachment = new Attachment([
            'post_id'   => $postId,
            'file_path' => $file->store('attachment'),
            'file_name' => $file->getClientOriginalName(),
        ]);

        $attachment->save();

        return $attachment;
    }

    /**
     * @param int $id
     *
     * @return bool
     *
     * @throws Exception
     */
    public function deleteAttachmentById(int $id): bool
    {
        if (!$attachment = Attachment::find($id)) {
            return false;
        }

        $user = auth()->user();

        if (!$user instanceof User) {
            throw new AuthorizationException();
        }
        if ($user->id !== (int) $attachment->post->user_id) {
            throw new AccessDeniedException();
        }

        return $attachment->delete();
    }

    /**
     * @param int $id
     *
     * @return Attachment|null
     */
    public function getAttachmentById(int $id): ?Attachment
    {
        return Attachment::find($id);
    }
}
