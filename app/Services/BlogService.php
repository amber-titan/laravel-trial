<?php

namespace App\Services;

use App\Models\Blog\Post;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

/**
 * Class BlogService
 *
 * @package App\Services
 */
class BlogService implements BlogServiceInterface
{
    /**
     * @var AttachmentsService
     */
    protected $attachmentsService;

    /**
     * @var Post
     */
    protected $post;

    /**
     * @param Post               $post
     * @param AttachmentsService $attachmentsService
     */
    public function __construct(Post $post, AttachmentsService $attachmentsService)
    {
        $this->post               = $post;
        $this->attachmentsService = $attachmentsService;
    }

    /**
     * @param Post $post
     *
     * @return array
     *
     * @throws Exception
     */
    public function postToArray(Post $post)
    {
        return [
            'id'          => $post->id,
            'user_id'     => $post->user_id,
            'title'       => $post->title,
            'content'     => $post->content,
            'tags'        => $post->tags()->pluck('tag'),
            'attachments' => $this->attachmentsService->attachmentsToArray($post->attachments),
            'created_at'  => (new Carbon($post->created_at))->format('Y-m-d H:i:s'),
            'updated_at'  => (new Carbon($post->updated_at))->format('Y-m-d H:i:s'),
        ];
    }

    /**
     * @param LengthAwarePaginator $posts
     *
     * @return array
     *
     * @throws Exception
     */
    public function postsToArray(LengthAwarePaginator $posts): array
    {
        $result = $posts->toArray();
        $data   = [];

        foreach ($posts->items() as $post) {
            $data[] = $this->postToArray($post);
        }

        $result['data'] = $data;

        return $result;
    }

    /**
     * Создание
     *
     * @param Request $request
     *
     * @return Post|Model
     */
    public function store(Request $request): Post
    {
        $data = [
            'user_id' => auth()->id(),
            'title'   => $request->input('title'),
            'content' => $request->input('content'),
        ];

        $post = $this->post->create($data);

        foreach ($request->input('tags', []) as $tag) {
            $post->tags()->create(['tag'=>$tag]);
        }

        return $post;
    }

    /**
     * @param Post $post
     * @param Request $request
     *
     * @return Post
     */
    public function update(Post $post, Request $request): Post
    {
        $data = [
            'title'   => $request->input('title', $post->title),
            'content' => $request->input('content', $post->content),
        ];

        $post->update($data);

        if($request->input('tags')) {
            $post->tags()->delete();
            foreach ($request->input('tags', []) as $tag) {
                $post->tags()->create(['tag'=>$tag]);
            }
        }


        $post->update($data);

        return $post;
    }

    /**
     * @param array $filter
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getPosts(array $filter = [], string $sort = 'desc'): LengthAwarePaginator
    {
        $posts = Post::with('tags');

        foreach ($filter as $key => $value) {
            switch ($key) {
                case "user_id":
                    $posts->where('user_id', $value);
                    break;
                case "tag":
                    $posts->whereHas('tags', function (Builder $query) use ($value) {
                        $query->where('tag', $value);
                    });
                    break;
                case "attachment":
                    if((boolean) $value) {
                        $posts->has('attachments');
                    }
                    break;
            }
        }

        return $posts->orderBy('created_at', $sort)->paginate();
    }
}
