<?php

namespace App\Providers;

use App\Models\Blog\Post;
use App\Services\BlogService;
use App\Services\BlogServiceInterface;
use Illuminate\Support\ServiceProvider;

/**
 * Class BlogServiceProvider
 *
 * @package App\Providers
 */
class BlogServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(BlogServiceInterface::class, BlogService::class);
    }
}
