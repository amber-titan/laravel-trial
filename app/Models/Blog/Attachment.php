<?php

declare(strict_types = 1);

namespace App\Models\Blog;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property-read int $id
 * @property int $post_id
 * @property string $file_path
 * @property string $file_name
 * @property-read Post $post
 * @mixin Eloquent
 */
class Attachment extends Model
{
    protected $table = 'blog_attachments';

    public $timestamps = false;

    protected $fillable = ['post_id', 'file_path', 'file_name'];

    /**
     * @return BelongsTo
     */
    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }
}
